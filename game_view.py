import arcade
import game_vm


def __stick__(x, y, angle, length=60):
    arcade.draw_rectangle_filled(x, y, length, 10, arcade.color.DARK_BROWN, angle)


def __circle__(x, y, radius):
    arcade.draw_circle_filled(x, y, radius, arcade.color.AERO_BLUE)


class MyGame(arcade.Window):
    """ Main application class. """
    X = 0
    Y = 0
    Angle = 1
    Core = None
    Speed = 10

    def init(self, width, height):
        super().init(width, height)
        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Set up your game here
        pass

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.__draw__()

        __stick__(self.X, self.Y, self.Angle, 120)
        __circle__(0, 0, 25)

    def __draw__(self):
        __stick__(self.X, self.Y, self.Angle, 120)
        __stick__(0, 60, 0, self.Speed)

        for stick in game_vm.Sticks:
            __stick__(800 - stick.X, stick.Y, stick.Angle)

        if self.Core is not None:
            if self.Core.X > 800 or self.Core.X < 0 or self.Core.Y < 0 or self.Core.Y > 600 or self.Core.speed <= 0:
                self.Core = None
            else:
                stick = game_vm.check()
                if stick:
                    self.Core.update_angle()
                else:
                    self.Core.update_coord()
            __circle__(self.Core.X, self.Core.Y, self.Core.Radius)

    def update(self, delta_time):
        """ All the logic to move, and the game logic goes here. """
        pass

    def on_key_press(self, key, modifiers):
        game_vm.check_key(key)
