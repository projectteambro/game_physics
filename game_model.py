import math


class ModelCore:
    X = 1
    Y = 1
    Radius = 5

    def __init__(self, x, y, angle, speed):
        self.time = 1
        self.count = 0
        self.X = x
        self.Y = y
        self.mass = 1
        self.speed = speed
        self.radius = 1
        self.damping = 0
        self.inertia = 2 / (self.radius * self.radius * self.mass * 3)
        self.vel = 1
        self.angelVel = 1
        self.angel = angle
        self.radAngle = 0
        self.delta_t = 0.000000001
        self.momentI = 0
        self.impulseMagn = 0
        self.add_count = 1
        self.ricochet = False
        self.znakX = 1
        self.znakY = 1
        self.ang_Vel = self.vel
        self.start_math()

    def start_math(self):
        v = self.speed
        g = 9.8

        self.radAngle = math.radians(self.angel)
        self.delta_t = 2 * self.speed * math.sin(self.radAngle) / 9.8
        x_t = v * self.delta_t * math.cos(self.radAngle) / 10  # расстояние приземленияснаряда
        y_max = v * self.delta_t / 2 * math.sin(
            self.radAngle) - g * self.delta_t * self.delta_t / 8  # максимальная высота полета снаряда
        length = 2 * math.sqrt(x_t * x_t / 4 + y_max * y_max)  # грубая оценка длины траектории
        num_frames = int(length / self.delta_t) * 10  # количество кадров для отрисовки
        self.delta_t = self.delta_t / (num_frames + 140)

    def update_coord(self):
        self.speed -= 0.001
        if not self.ricochet:
            self.X = self.speed * self.count * self.delta_t * math.cos(self.radAngle)
            self.Y = (self.speed * self.count * self.delta_t * math.sin(self.radAngle)) - (9.8 * self.count *
                                                                                           self.delta_t * self.count * (
                                                                                                   self.delta_t / 2))
        else:
            self.X += math.cos(self.radAngle)
            self.Y -= math.sin(self.radAngle)
#            self.X += math.cos(self.radAngle) * self.znakX
#            self.Y += math.sin(self.radAngle) * self.znakY

        self.count += self.add_count

    def update_angle(self):
        stepang_damping = 200
        self.ang_Vel = self.speed * (1.0 - stepang_damping / self.mass)

        self.angel = self.angel * self.ang_Vel

        self.radAngle = math.radians(self.angel)
        self.ricochet = True

        self.speed -= 0.005


class ModelStick:
    X = 0
    Y = 0
    Angle = 0
    Width = 10
    Height = 60

    def __init__(self, x, y, angle, x1, y1, x2, y2, x3, y3, x4, y4):
        self.X = x
        self.Y = y
        self.Angle = angle

        self.X1 = x1
        self.Y1 = y1
        self.X2 = x2
        self.Y2 = y2
        self.X3 = x3
        self.Y3 = y3
        self.X4 = x4
        self.Y4 = y4

        self.mass = 20
        self.damping = 2.0
        self.len = 3
        self.inertia = 1 / (self.len * self.len * self.mass * 12)
        self.vel = 0
        self.angelVel = 0
        self.speed = 0

    def update_cord(self):
        self.X += 3
        self.Y += 1 / 4
        self.speed -= 1
        self.vel = self.vel * (1.0 - 1000.0 * self.speed * self.damping / self.mass);
        self.angelVel = self.angelVel * (1.0 - 1000.0 * self.speed * self.damping / self.mass);
