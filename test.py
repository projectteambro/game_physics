# -*- coding: utf-8 -*-
import math
import time
import tkinter as tk

scene_width = 640
scene_height = 480
c = tk.Canvas(width=scene_width, height=scene_height, bg='grey80')
c.pack()
cannon_length = 70
v = 70
g = 9.8


def rotate_cannon(x_i, y_i):
    mn = y_i / x_i
    y = (mn * cannon_length) / (math.sqrt(mn * mn + 1))
    x = math.sqrt(cannon_length * cannon_length - y * y)
    c.coords(cn, 0, scene_height - 10, int(x), scene_height - 10 - int(y))
    angle = math.atan2(x_i, y_i)  # угол наклона ствола пушки
    t = 2 * v * math.sin(1.5707963267948966 - angle) / g
    x_t = v * t * math.cos(1.5707963267948966 - angle)  # координата прицела
    c.coords(cursor, int(x_t - 5), scene_height - 15, int(x_t + 5), scene_height - 5)
    return


def motion(event):
    if event.x > 0 and event.y < scene_height - 10:
        rotate_cannon(float(event.x), float(470 - event.y))
    return


def shoot_ball(event):
    ball = c.create_oval(-5, scene_height - 15, 5, scene_height - 5, fill='yellow')
    angle = math.atan2(float(event.x), float(470 - event.y))  # угол выстрела
    delta_t = 2 * v * math.sin(1.5707963267948966 - angle) / g  # время полета снаряда
    x_t = v * delta_t * math.cos(1.5707963267948966 - angle) / 10  # расстояние приземленияснаряда
    y_max = v * delta_t / 2 * math.sin(
        1.5707963267948966 - angle) - g * delta_t * delta_t / 8  # максимальная высота полета снаряда

    length = 2 * math.sqrt(x_t * x_t / 4 + y_max * y_max)  # грубая оценка длины траектории
    num_frames = int(length / delta_t)  # количество кадров для отрисовки
    delta_t = delta_t / num_frames
    for i in range(num_frames):  # цикл отрисовки
        x_ball = v * (i + 1) * delta_t * math.cos(1.5707963267948966 - angle)
        y_ball = v * (i + 1) * delta_t * math.sin(1.5707963267948966 - angle) - g * (i + 1) * delta_t * (i + 1) * delta_t / 2
        c.coords(ball, int(x_ball - 5), scene_height - y_ball - 15, int(x_ball + 5), scene_height-y_ball - 5)
        c.update()
        time.sleep(0.02)
    c.coords(ball, int(x_ball - 5), scene_height - y_ball - 15, int(x_ball + 5), scene_height - y_ball - 5)
    return


c.bind('<Motion>', motion)
c.bind('<Button 1>', shoot_ball)
c.create_oval(-25, 445, 25, 495, fill='#141414')
c.create_rectangle(25, scene_height - 10, 0, scene_height, fill='#141414')
cn = c.create_line(0, scene_height - 10, cannon_length, scene_height - 10, width=10, fill='#141414')
c.create_line(0, scene_height - 10, scene_width, scene_height - 10, width=1, fill='black')
cursor = c.create_oval(cannon_length, scene_height - 15, cannon_length + 10, scene_height-5, fill='red')
tk.mainloop()
