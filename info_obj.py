import fnmatch
import os


class Stick:
    for file in os.listdir('.'):
        if fnmatch.fnmatch(file, 'wood.png'):
            ...
        if fnmatch.fnmatch(file, 'rock.png'):
            ...
