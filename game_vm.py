import arcade
import math

import game_model
import game_view
import levels

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
game = game_view.MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
Sticks = []


def start_game():
    game.setup()
    load_stick()
    arcade.run()


def load_stick():
    lvl = levels.levels.lvl1

    coord_x = 250
    coord_y = 170
    for a in lvl:
        for b in a:
            if b == "|":
                add_sticks(coord_x, coord_y, 90, 10, 60)

            elif b == "_":
                add_sticks(coord_x, coord_y, 0, 10, 60)

            elif b == "\\":
                add_sticks(coord_x, coord_y, 120, 10, 60)

            elif b == "/":
                add_sticks(coord_x, coord_y, 60, 10, 60)

            coord_x -= 35
        coord_y -= 35
        coord_x = 250


def add_sticks(coord_x, coord_y, angle, width, height):
    x1 = 800 - (coord_x - height / 2)
    y1 = coord_y + width / 2

    x2 = 800 - (coord_x + height / 2)
    y2 = coord_y + width / 2

    x3 = 800 - (coord_x - height / 2)
    y3 = coord_y - width / 2

    x4 = 800 - (coord_x + height / 2)
    y4 = coord_y - width / 2

    rad_angle = math.radians(angle)

    X1 = (800 - coord_x) + (x1 - (800 - coord_x)) * math.cos(rad_angle) - (y1 - coord_y) * math.sin(rad_angle)
    Y1 = coord_y + (y1 - coord_y) * math.cos(rad_angle) + (x1 - (800 - coord_x)) * math.sin(rad_angle)

    X2 = (800 - coord_x) + (x2 - (800 - coord_x)) * math.cos(rad_angle) - (y2 - coord_y) * math.sin(rad_angle)
    Y2 = coord_y + (y2 - coord_y) * math.cos(rad_angle) + (x2 - (800 - coord_x)) * math.sin(rad_angle)

    X3 = (800 - coord_x) + (x3 - (800 - coord_x)) * math.cos(rad_angle) - (y3 - coord_y) * math.sin(rad_angle)
    Y3 = coord_y + (y3 - coord_y) * math.cos(rad_angle) + (x3 - (800 - coord_x)) * math.sin(rad_angle)

    X4 = (800 - coord_x) + (x4 - (800 - coord_x)) * math.cos(rad_angle) - (y4 - coord_y) * math.sin(rad_angle)
    Y4 = coord_y + (y4 - coord_y) * math.cos(rad_angle) + (x4 - (800 - coord_x)) * math.sin(rad_angle)

    Sticks.append(game_model.ModelStick(coord_x, coord_y, angle, X1, Y1, X2, Y2, X3, Y3, X4, Y4))


def check_key(key):
    if key == arcade.key.W:
        if game.Angle + 5 <= 70:
            game.Angle += 5
    elif key == arcade.key.S:
        if game.Angle - 5 >= 0:
            game.Angle -= 5
    elif key == arcade.key.SPACE:
        if game.Core is not None:
            return
        game.Core = game_model.ModelCore(game.X, game.Y + game.Angle / 3, game.Angle, game.Speed)
    elif key == arcade.key.A:
        if game.Speed - 10 >= 30:
            game.Speed -= 10
    elif key == arcade.key.D:
        if game.Speed + 10 <= 150:
            game.Speed += 10


def check_core():
    if self.Core is not None:
        if self.Core.X == 600:
            self.Core = None
        else:
            self.Core.update_coord()
            __circle__(self.Core.X, self.Core.Y, 5)


def length_core_and_stick(x, y):
    return math.sqrt(x * x + y * y)


def check():
    game.Core.update_coord()
    x = game.Core.X
    y = game.Core.Y
    for stick in Sticks:
        X = 800 - stick.X
        unrotated_circle_X = math.cos(math.radians(game.Core.angel)) * (x - X) - math.sin(
            math.radians(game.Core.angel)) * (y - stick.Y) + X
        unrotated_circle_Y = math.sin(math.radians(game.Core.angel)) * (x - X) + math.sin(
            math.radians(game.Core.angel)) * (y - stick.Y) + stick.Y

        closestX = 1
        closestY = 1

        if unrotated_circle_X < X:
            closestX = X
        elif unrotated_circle_X > X + 10:
            closestX = X + 10
        else:
            closestX = unrotated_circle_X

        if unrotated_circle_Y < stick.Y:
            closestY = stick.Y
        elif unrotated_circle_Y > stick.Y + 60:
            closestY = stick.Y + 60
        else:
            closestY = unrotated_circle_Y

        collision = False

        distance = find_distance(unrotated_circle_X, unrotated_circle_Y, closestX, closestY)
        if distance < 5:
            collision = True
            return collision
        else:
            collision = False

    return collision


def find_distance(from_x, from_y, to_x, to_y):
    a = math.fabs(from_x - to_x)
    b = math.fabs(from_y - to_y)

    return math.sqrt((a * a) + (b * b))
