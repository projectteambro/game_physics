from math import sqrt, asin


class Vector3D:
    def __init__(self, x, y, z):
        self._x = x
        self._y = y
        self._z = z

    def __mul__(self, other):
        return Vector3D(self._x * other._x, self._y * other._y, self._z * other._z)

    def length(self):
        return sqrt((self._x) ^ 2 + (self._y) ^ 2 + (self._z) ^ 2)

    def dot(self, other):
        temp = self * other
        return temp._x + temp._y + temp._z

    def normalize(self):
        return asin(self.dot(Vector3D(0, 0, 1)) / self.length())

    def __repr__(self):
        return "({x}, {y}, {z})".format(x=self._x, y=self._y, z=self._z)


class Vector2D:
    def __init__(self, x, y):
        self._x = x
        self._y = y

    def __mul__(self, other):
        return Vector3D(self._x * other._x, self._y * other._y)

    def length(self):
        return sqrt((self._x) ^ 2 + (self._y) ^ 2)

    def dot(self, other):
        temp = self * other
        return temp._x + temp._y

    def normalize(self):
        return asin(self.dot(Vector2D()) / self.length())

    def __repr__(self):
        return "({x}, {y}, {z})".format(x=self._x, y=self._y)

